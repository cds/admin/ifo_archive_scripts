# ifo_archive_scripts



## Overview

This collection of scripts will upload the DAQ filter files and channel lists to repos on git.ligo.org for archival purposes.  There are separate git repos for the channel and filter files, per IFO:

* https://git.ligo.org/cds/ifo/daq/channels/l1
* https://git.ligo.org/cds/ifo/daq/channels/h1
* https://git.ligo.org/cds/ifo/front-end/filters/l1
* https://git.ligo.org/cds/ifo/front-end/filters/h1


## How to use

###  Obtain an access token

Access tokens are used to provide robot write access to the git repositories.  You will need one access token for each repository that you need to push to.  Each IFO should only need 2 access tokens, as they won't need write access to the other IFO's config repos.

The access tokens should be created with the Developer role and the write_repository scope.

Once a token has been generated, save it into a file named `/etc/ifo_archive_scripts/access_tokens.txt`.  The file should be readable only by the user who will be running the update scripts.  The format of the file is:

```
$repo=$token
```

For example:

```
cds/ifo/daq/channels/l1=glpat-abcdefghijklmnopqrstuvwxyz
cds/ifo/front-end/filters/l1=glpat-abcdefghijklmnopqrstuvwxyz
```

### Cron job

Grab the cron driver script from this repository: https://git.ligo.org/cds/admin/ifo_archive_scripts/-/blob/main/daqgit_cron.sh?ref_type=heads

Run the cron driver script on a host that has the /opt/rtcds directory mounted.  Set the cron job to run daily.  Using a systemd timer instead of a cron job is an exercise left to the reader.


```
50 03 * * * /usr/local/home/llodaqsvn/daqgit_cron.sh
```

## Running by hand

If you would like to run the updater by hand:

```
git clone https://git.ligo.org/cds/admin/ifo_archive_scripts.git               
cd ifo_archive_scripts
./update_channel_archive.py
./update_filter_archive.py
```
