#!/bin/bash
source /ligo/cdscfg/stdenv.sh

# Get the scripts
export ARCTMP=`mktemp -d`
echo ' create temp dir ' $ARCTMP
cd $ARCTMP
git clone https://git.ligo.org/cds/admin/ifo_archive_scripts.git
cd ifo_archive_scripts
mkdir -p ~/archlog
echo 'update channel list'
./update_channel_archive.py > ~/archlog/daqchanlog.txt 2>&1
echo 'update filter list'
./update_filter_archive.py > ~/archlog/daqfiltlog.txt 2>&1
cd ~
echo ' clear temp dir'
rm -rf $ARCTMP
