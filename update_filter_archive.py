#!/usr/bin/python3 
import os
import sys
import subprocess
import filecmp
import shutil
import socket
from gpstime import gpstime

sys.path.append('/ligo/cdscfg')
import stdenv as cds
cds.INIT_ENV()


# Access token for authenticating with git.ligo.org
token_filename = "/etc/ifo_archive_scripts/access_tokens.txt"
my_key = f"cds/ifo/front-end/filters/{cds.ifo}"

GPS_file = "archive_GPS"
# Define constants (modify as needed)
GIT_HOST = "git.ligo.org"
GIT_GROUP = "cds/ifo/front-end/filters"
SOURCE_DIR = f"{cds.CHANSDIR}/filter_archive"  # Assuming full path based on initialization
GPS_FILE = "archive_GPS"
GIT_BRANCH = "main"
GIT_REPO = cds.ifo
GIT_IMAGEDIR = "filters"
# This must be an absolute path because it's used in chdir() calls
IMAGE_TOPDIR = "/var/tmp/ifo_archive"

def get_access_token(token_filename, key):
  """
  This function loads access tokens from a file and returns the value for a specific key.

  Args:
      token_filename (str): Path to the token file containing access tokens.
      key (str): The key for which to retrieve the value.

  Returns:
      str: The value associated with the key in the token file, or None if not found.
  """
  tokens = {}

  try:
    # Open the token file in read mode
    with open(token_filename, "r") as token_file:
      for line in token_file:
        # Remove trailing newline character
        line = line.strip()

        # Skip comment lines (starting with whitespace and '#')
        if not line.startswith("#"):
          # Split the line on colon (:) or equals sign (=)
          key_line, value = line.split("=", 1)

          # Check if both key and value are defined (avoid empty entries)
          if key_line and value:
            tokens[key_line] = value
          else:
            print(f"Ignoring invalid line: {line}")

  except FileNotFoundError:
    print(f"Error: Could not open token file {token_filename}")
    return None

  # Return the value for the requested key, or None if not found
  return tokens.get(key)

def get_last_line_from_file(filename):
  """
  This function reads the last line from a file and returns it.

  Args:
      filename (str): Path to the file to read.

  Returns:
      str: The last line of the file, or None if the file could not be opened.
  """

  try:
    # Open the file in read mode
    with open(filename, "r") as fh:
      # Read and return the last line
      return fh.readlines()[-1].strip()
  except FileNotFoundError:
    print(f"Error: Could not open file {filename}")
    return None

def is_valid_gps_time(gps_time, now_gps, oldest_gps):
  """
  This function checks if a GPS time is within the valid range.

  Args:
      gps_time (int): The GPS time to check.
      now_gps (int): The current GPS time.
      oldest_gps (int): The oldest valid GPS time.

  Returns:
      bool: True if the GPS time is within the valid range, False otherwise.
  """
  return oldest_gps <= gps_time <= now_gps

def get_iso_date_from_gps(gps_time):
  """
  This function converts a GPS time to an ISO 8601 formatted date string.

  Args:
      gps_time (int): The GPS time to convert.

  Returns:
      str: The ISO 8601 formatted date string, or None if conversion fails.
  """
  # Replace with actual implementation using your gpstime library/function
  # This example assumes gpstime provides a conversion function
  return gpstime.tconvert(gps_time)

access_token = get_access_token(token_filename, my_key)

#if access_token:
#  print(f"Access token for {my_key}: {access_token}")
#else:
#  print(f"Access token for {my_key} not found")


os.makedirs(IMAGE_TOPDIR, exist_ok=True)
os.chdir(IMAGE_TOPDIR)

# Remove any previous checkout
if os.path.exists(f"{IMAGE_TOPDIR}/{GIT_IMAGEDIR}"):
  shutil.rmtree(f"{IMAGE_TOPDIR}/{GIT_IMAGEDIR}")

# Build the git clone command string
co_string = f"git clone -b {GIT_BRANCH} https://autoupdater:{access_token}@{GIT_HOST}/{GIT_GROUP}/{GIT_REPO} {GIT_IMAGEDIR}"

# Print the command string for information
print(f"Cloning {co_string}")

# Execute the git clone command using subprocess
subprocess.run(co_string.split(), check=True)

filterfile_hash = {}
archivepath_hash = {}
isodate_hash = {}

image_dir = f"{IMAGE_TOPDIR}/{GIT_IMAGEDIR}"
os.chdir(image_dir)

last_gps = gpstime.parse(get_last_line_from_file(GPS_FILE))
if last_gps:
  print(f"Looking for archives newer than GPS {last_gps.gps()}")
else:
  print(f"Could not determine last GPS time (file {GPS_FILE} not found)")
nowGPS=gpstime.now().gps()
oldestGPS=gpstime.parse("1/1/2011").gps()


try:
  with os.scandir(SOURCE_DIR) as entries:
    # Loop through entries in the directory
    for entry in entries:
      # Check for directory entries (not '.' or '..')
      if entry.is_dir() and not entry.name.startswith("."):
        full_filterdir = os.path.join(SOURCE_DIR, entry.name)
        print(f"Start filters for {entry.name}")
        for filt_archive in os.listdir(full_filterdir):
            #print(f"Processing {filt_archive}")
            # Skip special entries ('.', '..') and installation archives
            if not filt_archive.startswith(".") and not filt_archive.endswith("_install.txt"):
              file_ext = None
              if filt_archive.endswith(".fir"):
                file_ext = ".fir"
              elif filt_archive.endswith(".txt"):
                file_ext = ".txt"
              else:
                continue  # Skip files with unsupported extensions

              # Extract filter filename and GPS time from archive name
              file_tokens = filt_archive.split("_")
              filterfile = file_tokens[0] + file_ext

              GPS = None
              iso_date = None

              if len(file_tokens) > 1:
                # Case 1: GPS time appended to filename
                if file_tokens[1].endswith(file_ext):
                  GPS = gpstime.fromgps(file_tokens[1][:-len(file_ext)])
                  if is_valid_gps_time(GPS.gps(), nowGPS, oldestGPS):
                    iso_date = GPS.iso()
                  else:
                    print(f"GPS string {int(GPS.gps())} is out of range, skipping")
                else:
                  # Case 2: Date/time string in filename
                  try:
                    year = int(file_tokens[1][:2]) + 2000
                    month = int(file_tokens[1][2:4])
                    day = int(file_tokens[1][4:6])
                    hour = int(file_tokens[2][:2])
                    minute = int(file_tokens[2][2:4])
                    second = int(file_tokens[2][4:6])
                    date_time = f"{year}-{month:02d}-{day:02d}T{hour:02d}:{minute:02d}:{second:02d}"
                    GPS = gpstime.parse(date_time)  # Replace with actual conversion
                    if is_valid_gps_time(GPS.gps(), nowGPS, oldestGPS):
                      iso_date = f"{date_time}.000000Z"
                    else:
                      print(f"Date string {date_time} is out of range, skipping")
                  except ValueError:
                    print(f"Invalid date/time format in {filt_archive}, skipping")
                    continue
                if iso_date:
                  filterfile_hash[int(GPS.gps())] = filterfile
                  archivepath_hash[int(GPS.gps())] = os.path.join(entry.name, filt_archive)
                  isodate_hash[int(GPS.gps())] = iso_date
              else:
                print(f"Unexpected filename format: {filt_archive}, skipping")
except OSError as e:
  print(f"Error: Could not access directory {SOURCE_DIR} ({e})")




# Calculate total number of archives
total_archives = len(archivepath_hash)
print(f"The total number of filterfile archives is {total_archives}")

# Filter archives based on GPS time
recent_keys = [key for key in archivepath_hash.keys() if int(key) > last_gps.gps()]
num_recent_archives = len(recent_keys)
print(f"Number of new filter file archives after {last_gps.gps()} is {num_recent_archives}")

# Exit if no new archives
if not num_recent_archives:
  print("NO NEW LISTS - Exiting")
  exit(0)

# Print information about recent archives
print("We found the following archives:")
for key in recent_keys:
  print(f"- {key}")

# Create dictionaries for recent archives
recent_filterfile_hash = {key: filterfile_hash[key] for key in recent_keys}
recent_archivepath_hash = {key: archivepath_hash[key] for key in recent_keys}
recent_isodate_hash = {key: isodate_hash[key] for key in recent_keys}

# Print recent archive details with dictionary comprehension
print("\nRecent archive details:")
for key in recent_archivepath_hash:
  print(f"{key}: {recent_archivepath_hash[key]}")

for gps_key in sorted(int(key) for key in recent_archivepath_hash):
  number_to_commit = 0

  full_target_file_name = os.path.join(SOURCE_DIR, recent_archivepath_hash[gps_key])
  this_file_name = recent_filterfile_hash[gps_key]
  full_image_file_name = os.path.join(image_dir, this_file_name)

  # Check if image file exists
  if os.path.exists(full_image_file_name):
    # Compare file contents
    if not filecmp.cmp(full_target_file_name, full_image_file_name, shallow=False):
      # Copy modified file
      try:
        print(f"Copying modified file: {full_target_file_name} -> {full_image_file_name} (this = {this_file_name})")
        shutil.copy2(full_target_file_name, full_image_file_name)
        print(f"Adding modified file to git: git add {this_file_name}")
        subprocess.run(["git", "add", this_file_name], check=True)
        number_to_commit += 1
      except OSError as e:
        print(f"Error copying file: {e}")

      number_to_commit += 1
  else:
    # Copy new file and add to Git
    try:
      print(f"Copying new file: {full_target_file_name} -> {full_image_file_name} (this = {this_file_name})")
      shutil.copy2(full_target_file_name, full_image_file_name)
      print(f"Adding new file to git: git add {this_file_name}")
      subprocess.run(["git", "add", this_file_name], check=True)
      number_to_commit += 1
    except OSError as e:
      print(f"Error copying file: {e}")

  # generate commit message and date
  commit_message = f"commit archive at gps {gps_key} from {recent_archivepath_hash[gps_key]}"
  commit_date = recent_isodate_hash[gps_key]

  # update git repository (if any files were committed)
  if number_to_commit > 0:
    with open(GPS_file, "w") as fh:
      fh.write(str(gps_key))

    try:
      subprocess.run(["git", "add", GPS_file], check=True)
      subprocess.run(["git", "commit", "-m", commit_message], check=True)
      subprocess.run(["git", "commit", "--amend", "--date", recent_isodate_hash[gps_key], "-m", commit_message], check=True)
      subprocess.run(["git", "push"])
    except subprocess.CalledProcessError as e:
      print(f"Error committing file: {e}")
      print(f"Standard error:\n{e.stderr.decode()}")
    except OSError as e:
      print(f"Error committing file: {e}")
  else:
    print(f"no changed files in {recent_archivepath_hash[gps_key]}")

